// Copyright 2017 Danny van Kooten. All rights reserved.
// Copyright 2020 Jim Kalafut.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package entemplate

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"hash/fnv"
	"html/template"
	"io"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"unsafe"
)

// Entemplate holds a reference to all templates
// and shared configuration like Delims or FuncMap
type Entemplate struct {
	shared       *template.Template
	sharedBackup *template.Template
	extendsRegex *regexp.Regexp
	templates    map[string]*template.Template
	root         string
	extensions   []string
	fileTreeHash uint64
	autoReload   bool
}

type templateFile struct {
	layout   string
	contents string
}

// New allocates a new, empty, template map
func New() *Entemplate {
	shared := template.New("")
	e := &Entemplate{
		shared:    shared,
		templates: make(map[string]*template.Template),
	}
	e.Delims("{{", "}}")
	return e
}

// Delims sets the action delimiters to the specified strings,
// to be used in subsequent calls to ParseDir.
// Nested template  definitions will inherit the settings.
// An empty delimiter stands for the corresponding default: {{ or }}.
// The return value is the template, so calls can be chained.
func (x *Entemplate) Delims(left, right string) *Entemplate {
	x.shared.Delims(left, right)
	p := regexp.QuoteMeta(left) + ` *?extends +?"(.+?)" *?` + regexp.QuoteMeta(right)
	x.extendsRegex = regexp.MustCompile(p)
	return x
}

// Funcs adds the elements of the argument map to the template's function map.
// It must be called before templates are parsed
// It panics if a value in the map is not a function with appropriate return
// type or if the name cannot be used syntactically as a function in a template.
// It is legal to overwrite elements of the map. The return value is the Entemplate instance,
// so calls can be chained.
func (x *Entemplate) Funcs(funcMap template.FuncMap) *Entemplate {
	x.shared.Funcs(funcMap)
	return x
}

// Lookup returns the template with the given name
// It returns nil if there is no such template or the template has no definition.
func (x *Entemplate) Lookup(name string) *template.Template {
	return x.templates[name]
}

func (x *Entemplate) Templates() map[string]*template.Template {
	return x.templates
}

// AutoReload configures whether the templates will be automatically reloaded
// from disk when they change. This setting has no effect when static templates
// are compiled into the source.
func (x *Entemplate) AutoReload(reload bool) *Entemplate {
	x.autoReload = reload
	return x
}

// ExecuteTemplate applies the template named name to the specified data object and writes the output to wr.
// Templates will be automatically reloaded if AutoReload is true.
// In addition to handling a single data object as is done in the standard library, this
// wrapper also supports providing key/value pairs as individual arguments. e.g.
//
// tpl.ExecuteTemplate(w, "template.html", "name", "Bob", "age", 42)
func (x *Entemplate) ExecuteTemplate(wr io.Writer, name string, data ...any) error {
	if x.autoReload {
		if err := x.reloadTemplates(); err != nil {
			return err
		}
	}

	tmpl := x.Lookup(name)
	if tmpl == nil {
		return fmt.Errorf("extemplate: no template %q", name)
	}

	switch len(data) {
	case 0:
		return tmpl.Execute(wr, nil)
	case 1:
		return tmpl.Execute(wr, data[0])
	}

	if len(data)%2 != 0 {
		return errors.New("odd number of key/value pairs as template data")
	}

	dataMap := make(map[string]any, len(data)/2)
	for i := 0; i < len(data); i += 2 {
		key, ok := data[i].(string)
		if !ok {
			return errors.New("template data key must be a string")
		}
		dataMap[key] = data[i+1]
	}
	return tmpl.Execute(wr, dataMap)
}

// ParseFS 支持解析内嵌的资源, 但是并不支持重载
func (x *Entemplate) ParseFS(tplFs fs.FS, root string, extensions []string) error {
	root = strings.TrimRight(root, "/") + "/"
	if len(extensions) == 0 {
		extensions = []string{".html", ".tmpl", ".gohtml"}
	}
	exts := make(map[string]bool, len(extensions))
	for _, e := range extensions {
		exts[e] = true
	}

	files := make(map[string]*templateFile)
	err := fs.WalkDir(tplFs, strings.TrimRight(root, "/"), func(fullPath string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			return nil
		}
		// skip if extension not in list of allowed extensions
		if !exts[path.Ext(fullPath)] {
			return nil
		}
		contents, err := fs.ReadFile(tplFs, fullPath)
		if err != nil {
			return err
		}
		name := strings.TrimPrefix(fullPath, root)
		files[name], err = x.newTemplateFile(contents)
		return err
	})
	if err != nil {
		return err
	}
	return x.parseTemplates(files)
}

// ParseDir walks the given directory root and parses all files with any of the registered extensions.
// Default extensions are .html and .tmpl
// If a template file has {{/* extends "other-file.tmpl" */}} as its first line it will parse that file for base templates.
// Parsed templates are named relative to the given root directory
func (x *Entemplate) ParseDir(root string, extensions []string) error {
	fi, err := os.Stat(root)
	if err != nil {
		return err
	}
	if !fi.IsDir() {
		return fmt.Errorf("%s is not a directory", root)
	}

	root = filepath.FromSlash(strings.TrimRight(root, "\\/")) + string(os.PathSeparator)
	x.root = root

	if len(extensions) == 0 {
		extensions = []string{".gohtml", ".tmpl", ".html"}
	}
	x.extensions = extensions

	fileList, hash, err := buildFileList(root, extensions)
	if err != nil {
		return err
	}

	files, err := x.loadTemplateFiles(root, fileList)
	if err != nil {
		return err
	}
	x.fileTreeHash = hash
	return x.parseTemplates(files)
}

func (x *Entemplate) parseTemplates(files map[string]*templateFile) error {
	if x.sharedBackup == nil {
		var err error
		if x.sharedBackup, err = x.shared.Clone(); err != nil {
			return err
		}
	}

	// parse all non-child templates into the shared template namespace
	for name, tf := range files {
		if tf.layout != "" {
			continue
		}

		_, err := x.shared.New(name).Parse(tf.contents)
		if err != nil {
			return err
		}
	}

	// then, parse all templates again but with inheritance
	for name, tf := range files {
		// if this is a non-child template, no need to reparse
		if tf.layout == "" {
			x.templates[name] = x.shared.Lookup(name)
			continue
		}

		tmpl := template.Must(x.shared.Clone()).New(name)

		// add to set under normalized name (path from root)
		x.templates[name] = tmpl

		// parse parent templates
		templateFiles := []string{name}
		pname := tf.layout
		parent, parentExists := files[pname]
		for parentExists {
			templateFiles = append(templateFiles, pname)
			pname = parent.layout
			parent, parentExists = files[parent.layout]
		}

		// parse template files in reverse order (because children should override parents)
		for j := len(templateFiles) - 1; j >= 0; j-- {
			_, err := tmpl.Parse(files[templateFiles[j]].contents)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func buildFileList(root string, extensions []string) ([]string, uint64, error) {
	exts := make(map[string]bool, len(extensions))

	// create map of allowed extensions
	for _, e := range extensions {
		exts[e] = true
	}

	fh := fnv.New64()
	files := make([]string, 0, 16)
	// find all template files
	err := filepath.WalkDir(root, func(path string, info fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		// skip dirs as they can never be valid templates
		if info == nil || info.IsDir() {
			return nil
		}

		// skip if extension not in list of allowed extensions
		if !exts[filepath.Ext(path)] {
			return nil
		}

		// incorporate path and modification time into hash for
		// detecting updates for auto reload
		fileInfo, _ := info.Info()
		_, _ = fh.Write(str2bytes(path))
		var b [8]byte
		binary.LittleEndian.PutUint64(b[:], uint64(fileInfo.ModTime().Unix()))
		_, _ = fh.Write(b[:])
		files = append(files, path)
		return nil
	})
	if err != nil {
		return nil, 0, err
	}
	return files, fh.Sum64(), nil
}

func (x *Entemplate) loadTemplateFiles(root string, paths []string) (map[string]*templateFile, error) {
	// load all template files
	files := make(map[string]*templateFile, len(paths))

	for _, path := range paths {
		contents, err := os.ReadFile(path)
		if err != nil {
			return nil, err
		}
		tf, err := x.newTemplateFile(contents)
		if err != nil {
			return nil, err
		}
		name := strings.TrimPrefix(path, root)
		name = strings.ReplaceAll(name, "\\", "/")
		files[name] = tf
	}

	return files, nil
}

func (x *Entemplate) reloadTemplates() error {
	fileList, hash, err := buildFileList(x.root, x.extensions)
	if err != nil {
		return err
	}
	if hash == x.fileTreeHash {
		return nil
	}

	files, err := x.loadTemplateFiles(x.root, fileList)
	if err != nil {
		return err
	}

	x.fileTreeHash = hash
	x.shared, err = x.sharedBackup.Clone()
	if err != nil {
		return err
	}

	x.templates = make(map[string]*template.Template)
	return x.parseTemplates(files)
}

// newTemplateFile parses the file contents into something that text/template can understand
func (x *Entemplate) newTemplateFile(contents []byte) (*templateFile, error) {
	tf := &templateFile{contents: bytes2str(bytes.TrimSpace(contents))}

	i := strings.IndexByte(tf.contents, '\n')
	if i < 10 {
		return tf, nil
	}
	if m := x.extendsRegex.FindStringSubmatch(tf.contents[:i]); m != nil {
		tf.layout = m[1]
		tf.contents = strings.TrimSpace(tf.contents[i+1:])
	}
	return tf, nil
}

func bytes2str(bs []byte) string {
	return unsafe.String(unsafe.SliceData(bs), len(bs))
}

func str2bytes(s string) []byte {
	return unsafe.Slice(unsafe.StringData(s), len(s))
}
